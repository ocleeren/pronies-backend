'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

const moment = require('moment');

module.exports = {
    /**
     * Create a record.
     *
     * @return {Object}
     */

    async create(ctx) {
        let entity;

        if (!ctx.state.user || !ctx.state.user.id) {
            return ctx.unauthorized(`You need to be logged in`);
        }

        if (moment('11/06/2021 21:00', 'DD/MM/YYYY HH:mm').isBefore(moment.now())) {
            return ctx.unauthorized(`Tijdstip voor invullen van bonusvragen is verlopen.`);
        };

        ctx.request.body.author = ctx.state.user.id;
        entity = await strapi.services['bonus-question-top-scorer-answer'].create(ctx.request.body);
        return sanitizeEntity(entity, { model: strapi.models['bonus-question-top-scorer-answer'] });
    },

    /**
     * Update a record.
     *
     * @return {Object}
     */

    async update(ctx) {
        const { id } = ctx.params;

        let entity;

        if (!ctx.state.user || !ctx.state.user.id) {
            return ctx.unauthorized(`You need to be logged in`);
        }

        if (moment('11/06/2021 21:00', 'DD/MM/YYYY HH:mm').isBefore(moment.now())) {
            return ctx.unauthorized(`Tijdstip voor invullen van bonusvragen is verlopen.`);
        };

        const [bonus_question_top_scorer_answer] = await strapi.services['bonus-question-top-scorer-answer'].find({
            id: ctx.params.id,
            'author.id': ctx.state.user.id,
        });

        if (!bonus_question_top_scorer_answer) {
            return ctx.unauthorized(`You can't update this entry`);
        }

        /**
         * TODO
        if (prono.game && moment(prono.game.date).isBefore(moment.now())) {
            return ctx.unauthorized(`Deze match is al bezig.`);
        };
        */

        entity = await strapi.services['bonus-question-top-scorer-answer'].update({ id }, ctx.request.body);

        return sanitizeEntity(entity, { model: strapi.models['bonus-question-top-scorer-answer'] });
    },

    /**
     * Count records.
     *
     * @return {Number}
     */

    count(ctx) {
        ctx.query['author.id'] = ctx.state.user.id;

        if (ctx.query._q) {
            return strapi.services['bonus-question-top-scorer-answer'].countSearch(ctx.query);
        }
        return strapi.services.prono.count(ctx.query);
    },

    /**
     * Retrieve records.
     *
     * @return {Array}
     */

    async find(ctx) {
        let entities;

        ctx.query['author.id'] = ctx.state.user.id;

        if (ctx.query._q) {
            entities = await strapi.services['bonus-question-top-scorer-answer'].search(ctx.query);
        } else {
            entities = await strapi.services['bonus-question-top-scorer-answer'].find(ctx.query);
        }

        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models['bonus-question-top-scorer-answer'] }));
    },

    /**
     * Retrieve a record.
     *
     * @return {Object}
     */

    async findOne(ctx) {
        const { id } = ctx.params;

        const entity = await strapi.services['bonus-question-top-scorer-answer'].findOne({
            id,
            'author.id': ctx.state.user.id
        });

        return sanitizeEntity(entity, { model: strapi.models['bonus-question-top-scorer-answer'] });
    },
};
