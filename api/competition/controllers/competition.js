'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
const _ = require('lodash');
const axios = require('axios');
const slugify = require('slugify');

var secret = '6LfWofQaAAAAAMn5yJnVjcx81mvLNcj4i6vC2bzI';

async function makeid(length) {
    var result = [];
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }

    result = result.join('');

    const [competition] = await strapi.services.competition.find({
        Code: result,
    });

    if (competition && competition.length > 0) {
        makeid(8);
    }

    return result;
}

module.exports = {
    /**
     * Create a record.
     *
     * @return {Object}
     */

    async create(ctx) {
        let entity;

        const [exisiting] = await strapi.services.competition.find({
            slug: slugify(ctx.request.body.Title, { lower: true, strict: true }),
        });

        if (exisiting) {
            return ctx.unauthorized(`Er is al een competitie met deze naam.`);
        }

        const gres = await axios.post(`https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${ctx.request.body.token}`);

        if (!gres.data.success) {
            return ctx.unauthorized(`Please provide a valid reCaptcha token.`);
        }

        let code = await makeid(8);

        console.log(code);

        ctx.request.body.Code = code;

        entity = await strapi.services.competition.create(ctx.request.body);

        await strapi.services['competition-users'].create({
            user: ctx.state.user.id,
            competition: entity.id,
            Accepted: true,
            Admin: true
        });

        return sanitizeEntity(entity, { model: strapi.models.competition });
    },


    async delete(ctx) {
        const { id } = ctx.params;

        const entity = await strapi.services.competition.delete({ id });

        const competition_users = await strapi.services['competition-users'].find({ competition: id })

        competition_users.forEach(async (competition_user) => {
            await strapi.services.competition.delete({ id: competition_user.id });
        })

        return sanitizeEntity(entity, { model: strapi.models.competition });
    },
};
