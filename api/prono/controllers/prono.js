'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const { sanitizeEntity } = require('strapi-utils');

const moment = require('moment');

module.exports = {
    /**
     * Create a record.
     *
     * @return {Object}
     */

    async create(ctx) {
        let entity;
        ctx.request.body.author = ctx.state.user.id;
        entity = await strapi.services.prono.create(ctx.request.body);
        return sanitizeEntity(entity, { model: strapi.models.prono });
    },

    /**
     * Update a record.
     *
     * @return {Object}
     */

    async update(ctx) {
        const { id } = ctx.params;

        let entity;

        const [prono] = await strapi.services.prono.find({
            id: ctx.params.id,
            'author.id': ctx.state.user.id
        });

        if (!prono) {
            return ctx.unauthorized(`You can't update this entry`);
        }

        if (prono.game && moment(prono.game.date).isBefore(moment.now())) {
            return ctx.unauthorized(`Deze match is al bezig.`);
        };

        entity = await strapi.services.prono.update({ id }, ctx.request.body);

        return sanitizeEntity(entity, { model: strapi.models.prono });
    },

    /**
     * Count records.
     *
     * @return {Number}
     */

    count(ctx) {
        if (ctx.query._q) {
            return strapi.services.prono.countSearch(ctx.query);
        }
        return strapi.services.prono.count(ctx.query);
    },

    /**
     * Retrieve records.
     *
     * @return {Array}
     */

    async find(ctx) {
        let entities;

        if (ctx.query._q) {
            entities = await strapi.services.prono.search(ctx.query, ['competition.competition_users.user.email', 'author', 'game', 'game.team_home', 'game.team_away', 'competition']);
        } else {
            entities = await strapi.services.prono.find(ctx.query, ['competition.competition_users.user.email', 'author', 'game', 'game.team_home', 'game.team_away', 'competition']);
        }

        entities = entities.filter((entity) => {
            return moment(entity.game.date).isBefore(moment.now()) || entity.author.id === ctx.state.user.id;
        });

        entities.forEach((entity) => {
            if (entity.game) {
                let game = entity.game;
                if (moment(game.date).isBefore(moment.now())) {
                    if (game.id === entity.game.id && parseInt(game.score_home) >= 0 && parseInt(game.score_away) >= 0) {
                        entity.pointsEarned = 0;
                        entity.scoreCorrect = false;
                        entity.correct = false;
                        if (entity.score_home === game.score_home && entity.score_away === game.score_away) {
                            if (entity.game.phase === 1) {
                                entity.pointsEarned = 5;
                            } else if (entity.game.phase === 2) {
                                entity.pointsEarned = 6;
                            } else if (entity.game.phase === 3) {
                                entity.pointsEarned = 7;
                            } else if (entity.game.phase === 4) {
                                entity.pointsEarned = 8;
                            } else if (entity.game.phase === 5) {
                                entity.pointsEarned = 10;
                            }
                            entity.scoreCorrect = true;
                        } else {
                            var homeWonGame = game.score_home > game.score_away;
                            var awayWonGame = game.score_home < game.score_away;
                            var equalGame = game.score_home === game.score_away;

                            var homeWonentity = entity.score_home > entity.score_away;
                            var awayWonentity = entity.score_home < entity.score_away;
                            var equalentity = entity.score_home === entity.score_away;

                            if ((homeWonentity && homeWonGame) || (awayWonentity && awayWonGame) || (equalGame && equalentity)) {
                                if (entity.game.phase === 1) {
                                    entity.pointsEarned = 3;
                                } else if (entity.game.phase === 2) {
                                    entity.pointsEarned = 4;
                                } else if (entity.game.phase === 3) {
                                    entity.pointsEarned = 5;
                                } else if (entity.game.phase === 4) {
                                    entity.pointsEarned = 6;
                                } else if (entity.game.phase === 5) {
                                    entity.pointsEarned = 8;
                                }
                                entity.correct = true;
                            }
                        }
                    }
                }
            }
        })

        entities = entities.sort((a, b) => {
            return a.pointsEarned < b.pointsEarned ? 1 : -1;
        });

        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.prono }));
    },

    /**
     * Retrieve a record.
     *
     * @return {Object}
     */

    async findOne(ctx) {
        const { id } = ctx.params;

        const entity = await strapi.services.prono.findOne({
            id
        }, ['competition.competition_users.user.email', 'author', 'game', 'competition']);

        return sanitizeEntity(entity, { model: strapi.models.prono });
    },

    async getKlassement(ctx) {
        let games;
        let pronos;

        ctx.query._limit = -1;

        if (ctx.query._q) {
            games = await strapi.services.game.search();
            pronos = await strapi.services.prono.search(ctx.query);
        } else {
            games = await strapi.services.game.find();
            pronos = await strapi.services.prono.find(ctx.query);
        }

        let [competition] = await strapi.services.competition.find({ slug: ctx.query['competition.slug'] }, ['competition_users.user.username']);

        let klassement = [];

        competition.competition_users.forEach(async (compUser) => {
            if (compUser.Accepted === true) {
                var hasEmail = klassement.some((klass) => {
                    return klass.username === compUser.user.username;
                });
                console.log(hasEmail);
                if (!hasEmail) {
                    console.log(compUser.user.username);
                    let [bonusQuestionGoalsAnswer] = await strapi.services['bonus-question-goal-answer'].find({ author: compUser.user.id, competition: competition.id, _limit: -1  });
                    klassement.push({ competition: competition.Title, username: compUser.user.username, score: 0, amountOfMatchesScoreCorrect: 0, amountOfMatchesCorrect: 0, bonusQuestionGoals: bonusQuestionGoalsAnswer ? bonusQuestionGoalsAnswer.Goals : 0 })
                }
            }
        })

        function addScore(username, score, matchScoreCorrect, matchCorrect) {
            console.log(`Add Score ${score} ${username}`);

            klassement.map((klass) => {
                if (klass.username === username) {
                    klass.score = klass.score + score;
                    if (matchScoreCorrect) {
                        klass.amountOfMatchesScoreCorrect += 1;
                    }
                    if (matchCorrect) {
                        klass.amountOfMatchesCorrect += 1;
                    }
                }
            })
        }

        let [bonusQuestionWinner] = await strapi.services['bonus-question-winner'].find({ team_null: false });
        let bonusQuestionWinnerAnswers = await strapi.services['bonus-question-winner-answer'].find({ team_null: false, _limit: -1  });

        bonusQuestionWinnerAnswers.forEach((bonusQuestionWinnerAnswer) => {
            if (competition.id === bonusQuestionWinnerAnswer.competition.id && bonusQuestionWinner && bonusQuestionWinner.team.id === bonusQuestionWinnerAnswer.team.id) {
                addScore(bonusQuestionWinnerAnswer.author.username, 10, false, false);
            }
        })

        let [bonusQuestionsTopScorer] = await strapi.services['bonus-question-top-scorer'].find({ rank: 1, top_scorer_null: false });
        let bonusQuestionsTopScorerAnswers = await strapi.services['bonus-question-top-scorer-answer'].find({ top_scorer_null: false, competition_null: false, _limit: -1  });

        if (bonusQuestionsTopScorer) {
            bonusQuestionsTopScorerAnswers.forEach((bonusQuestionsTopScorerAnswer) => {
                if (competition.id === bonusQuestionsTopScorerAnswer.competition.id) {
                    if (bonusQuestionsTopScorerAnswer.bonus_question_top_scorer.id === 1 && bonusQuestionsTopScorer.top_scorer.id === bonusQuestionsTopScorerAnswer.top_scorer.id) {
                        addScore(bonusQuestionsTopScorerAnswer.author.username, 7, false, false);
                    } else if (bonusQuestionsTopScorerAnswer.bonus_question_top_scorer.id === 2 && bonusQuestionsTopScorer.top_scorer.id === bonusQuestionsTopScorerAnswer.top_scorer.id) {
                        addScore(bonusQuestionsTopScorerAnswer.author.username, 5, false, false);
                    } else if (bonusQuestionsTopScorerAnswer.bonus_question_top_scorer.id === 3 && bonusQuestionsTopScorer.top_scorer.id === bonusQuestionsTopScorerAnswer.top_scorer.id) {
                        addScore(bonusQuestionsTopScorerAnswer.author.username, 3, false, false);
                    }
                }
            })
        }

        games.forEach((game) => {
            if (moment(game.date).isBefore(moment.now())) {
                pronos.forEach((prono) => {
                    if (game.id === prono.game.id && parseInt(game.score_home) >= 0 && parseInt(game.score_away) >= 0) {
                        if (prono.score_home === game.score_home && prono.score_away === game.score_away) {
                            if (prono.game.phase === 1) {
                                addScore(prono.author.username, 5, true, false);
                            } else if (prono.game.phase === 2) {
                                addScore(prono.author.username, 6, true, false);
                            } else if (prono.game.phase === 3) {
                                addScore(prono.author.username, 7, true, false);
                            } else if (prono.game.phase === 4) {
                                addScore(prono.author.username, 8, true, false);
                            } else if (prono.game.phase === 5) {
                                addScore(prono.author.username, 10, true, false);
                            }
                        } else {
                            var homeWonGame = game.score_home > game.score_away;
                            var awayWonGame = game.score_home < game.score_away;
                            var equalGame = game.score_home === game.score_away;

                            var homeWonProno = prono.score_home > prono.score_away;
                            var awayWonProno = prono.score_home < prono.score_away;
                            var equalProno = prono.score_home === prono.score_away;

                            if ((homeWonProno && homeWonGame) || (awayWonProno && awayWonGame) || (equalGame && equalProno)) {
                                if (prono.game.phase === 1) {
                                    addScore(prono.author.username, 3, false, true);
                                } else if (prono.game.phase === 2) {
                                    addScore(prono.author.username, 4, false, true);
                                } else if (prono.game.phase === 3) {
                                    addScore(prono.author.username, 5, false, true);
                                } else if (prono.game.phase === 4) {
                                    addScore(prono.author.username, 6, false, true);
                                } else if (prono.game.phase === 5) {
                                    addScore(prono.author.username, 8, false, true);
                                }
                            }
                        }
                    }
                });
            }
        });

        let [bonusQuestionGoals] = await strapi.services['bonus-question-goals'].find({ _limit: -1 });
        let bonusQuestionGoalsAmount = bonusQuestionGoals ? bonusQuestionGoals.Goals : 0;

        klassement = klassement.sort((a, b) => {
            if (a.score === b.score) {
                if (a.amountOfMatchesScoreCorrect === b.amountOfMatchesScoreCorrect) {
                    return Math.abs(bonusQuestionGoalsAmount - a.bonusQuestionGoals) - Math.abs(bonusQuestionGoalsAmount - b.bonusQuestionGoals);
                }
                return a.amountOfMatchesScoreCorrect < b.amountOfMatchesScoreCorrect ? 1 : -1;
            }
            return a.score < b.score ? 1 : -1;
        });

        return klassement;
    },

    async getKlassementFull(ctx) {
        let games;
        let pronos;

        ctx.query._limit = -1;

        if (ctx.query._q) {
            games = await strapi.services.game.search();
            pronos = await strapi.services.prono.search(ctx.query);
        } else {
            games = await strapi.services.game.find();
            pronos = await strapi.services.prono.find(ctx.query);
        }

        let competitions = await strapi.services.competition.find({}, ['competition_users.user.username']);

        let klassement = [];

        competitions.forEach((competition) => {
            competition.competition_users.forEach(async (compUser) => {
                if (compUser.Accepted === true) {
                    var hasEmail = klassement.some((klass) => {
                        return klass.username === compUser.user.username && klass.competition === competition.Title;
                    });
                    if (!hasEmail) {
                        let [bonusQuestionGoalsAnswer] = await strapi.services['bonus-question-goal-answer'].find({ author: compUser.user.id, competition: competition.id, _limit: -1  });
                        klassement.push({ competition: competition.Title, username: compUser.user.username, score: 0, amountOfMatchesScoreCorrect: 0, amountOfMatchesCorrect: 0, bonusQuestionGoals: bonusQuestionGoalsAnswer ? bonusQuestionGoalsAnswer.Goals : 0 })
                    }
                }
            })
        })

        function addScore(competition, username, score, matchScoreCorrect, matchCorrect) {
            klassement.forEach((klass) => {
                if (klass.username === username && klass.competition === competition) {
                    klass.score = klass.score + score;
                    if (matchScoreCorrect) {
                        klass.amountOfMatchesScoreCorrect += 1;
                    }
                    if (matchCorrect) {
                        klass.amountOfMatchesCorrect += 1;
                    }
                }
            })
        }

        let [bonusQuestionWinner] = await strapi.services['bonus-question-winner'].find({ team_null: false });
        let bonusQuestionWinnerAnswers = await strapi.services['bonus-question-winner-answer'].find({ team_null: false, _limit: -1 });

        bonusQuestionWinnerAnswers.forEach((bonusQuestionWinnerAnswer) => {
            if (bonusQuestionWinner && bonusQuestionWinner.team.id === bonusQuestionWinnerAnswer.team.id) {
                addScore(bonusQuestionWinnerAnswer.competition.Title, bonusQuestionWinnerAnswer.author.username, 10, false, false);
            }
        })

        let [bonusQuestionsTopScorer] = await strapi.services['bonus-question-top-scorer'].find({ rank: 1, top_scorer_null: false });
        let bonusQuestionsTopScorerAnswers = await strapi.services['bonus-question-top-scorer-answer'].find({ top_scorer_null: false, competition_null: false, _limit: -1 });

        if (bonusQuestionsTopScorer) {
            bonusQuestionsTopScorerAnswers.forEach((bonusQuestionsTopScorerAnswer) => {
                if (bonusQuestionsTopScorerAnswer.bonus_question_top_scorer.id === 1 && bonusQuestionsTopScorer.top_scorer.id === bonusQuestionsTopScorerAnswer.top_scorer.id) {
                    addScore(bonusQuestionsTopScorerAnswer.competition.Title, bonusQuestionsTopScorerAnswer.author.username, 7, false, false);
                } else if (bonusQuestionsTopScorerAnswer.bonus_question_top_scorer.id === 2 && bonusQuestionsTopScorer.top_scorer.id === bonusQuestionsTopScorerAnswer.top_scorer.id) {
                    addScore(bonusQuestionsTopScorerAnswer.competition.Title, bonusQuestionsTopScorerAnswer.author.username, 5, false, false);
                } else if (bonusQuestionsTopScorerAnswer.bonus_question_top_scorer.id === 3 && bonusQuestionsTopScorer.top_scorer.id === bonusQuestionsTopScorerAnswer.top_scorer.id) {
                    addScore(bonusQuestionsTopScorerAnswer.competition.Title, bonusQuestionsTopScorerAnswer.author.username, 3, false, false);
                }
            })
        }

        games.forEach((game) => {
            if (moment(game.date).isBefore(moment.now())) {
                pronos.forEach((prono) => {
                    if (prono && prono.competition && game.id === prono.game.id && parseInt(game.score_home) >= 0 && parseInt(game.score_away) >= 0) {
                        if (prono.score_home === game.score_home && prono.score_away === game.score_away) {
                            if (prono.game.phase === 1) {
                                addScore(prono.competition.Title, prono.author.username, 5, true, false);
                            } else if (prono.game.phase === 2) {
                                addScore(prono.competition.Title, prono.author.username, 6, true, false);
                            } else if (prono.game.phase === 3) {
                                addScore(prono.competition.Title, prono.author.username, 7, true, false);
                            } else if (prono.game.phase === 4) {
                                addScore(prono.competition.Title, prono.author.username, 8, true, false);
                            } else if (prono.game.phase === 5) {
                                addScore(prono.competition.Title, prono.author.username, 10, true, false);
                            }
                        } else {
                            var homeWonGame = game.score_home > game.score_away;
                            var awayWonGame = game.score_home < game.score_away;
                            var equalGame = game.score_home === game.score_away;

                            var homeWonProno = prono.score_home > prono.score_away;
                            var awayWonProno = prono.score_home < prono.score_away;
                            var equalProno = prono.score_home === prono.score_away;

                            if ((homeWonProno && homeWonGame) || (awayWonProno && awayWonGame) || (equalGame && equalProno)) {
                                if (prono.game.phase === 1) {
                                    addScore(prono.competition.Title, prono.author.username, 3, false, true);
                                } else if (prono.game.phase === 2) {
                                    addScore(prono.competition.Title, prono.author.username, 4, false, true);
                                } else if (prono.game.phase === 3) {
                                    addScore(prono.competition.Title, prono.author.username, 5, false, true);
                                } else if (prono.game.phase === 4) {
                                    addScore(prono.competition.Title, prono.author.username, 6, false, true);
                                } else if (prono.game.phase === 5) {
                                    addScore(prono.competition.Title, prono.author.username, 8, false, true);
                                }
                            }
                        }
                    }
                });
            }
        });

        let [bonusQuestionGoals] = await strapi.services['bonus-question-goals'].find({ _limit: -1 });
        let bonusQuestionGoalsAmount = bonusQuestionGoals ? bonusQuestionGoals.Goals : 0;

        klassement = klassement.sort((a, b) => {
            if (a.score === b.score) {
                if (a.amountOfMatchesScoreCorrect === b.amountOfMatchesScoreCorrect) {
                    return Math.abs(bonusQuestionGoalsAmount - a.bonusQuestionGoals) - Math.abs(bonusQuestionGoalsAmount - b.bonusQuestionGoals);
                }
                return a.amountOfMatchesScoreCorrect < b.amountOfMatchesScoreCorrect ? 1 : -1;
            }
            return a.score < b.score ? 1 : -1;
        });

        return klassement;
    }
};