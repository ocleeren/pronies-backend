'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
    /**
     * Create a record.
     *
     * @return {Object}
     */

    async create(ctx) {
        let entity;


        const [competition] = await strapi.services.competition.find({
            Code: ctx.request.body.Code,
        });

        const [competition_user] = await strapi.services['competition-users'].find({
            competition: competition.id,
            'competition.competition_users.user.id': ctx.state.user.id,
        });

        if (competition_user) {
            return ctx.unauthorized(`U neemt al deel aan deze competitie`);
        }

        if (competition) {
            ctx.request.body.user = ctx.state.user.id;
            ctx.request.body.competition = competition.id;
            ctx.request.body.Accepted = false;
            ctx.request.body.Admin = false;
            entity = await strapi.services['competition-users'].create(ctx.request.body);

            return sanitizeEntity(entity, { model: strapi.models['competition-users'] });
        } else {
            return ctx.unauthorized(`Geen competitie gevonden met deze code`);
        }

    },

    /**
     * Update a record.
     *
     * @return {Object}
     */

    async update(ctx) {
        const { id } = ctx.params;

        let entity;

        const [competition_user] = await strapi.services['competition-users'].find({
            id: ctx.params.id,
            'competition.competition_users.user.id': ctx.state.user.id,
            'competition.competition_users.Admin': true,
        });

        if (!competition_user) {
            return ctx.unauthorized(`You can't update this entry`);
        }

        entity = await strapi.services['competition-users'].update({ id }, ctx.request.body);

        return sanitizeEntity(entity, { model: strapi.models['competition-users'] });
    },

    /**
     * Count records.
     *
     * @return {Number}
     */

    count(ctx) {
        ctx.query['competition.competition_users.user.id'] = ctx.state.user.id;
        ctx.query['competition.competition_users.Admin'] = true;

        if (ctx.query._q) {
            return strapi.services.prono.countSearch(ctx.query);
        }
        return strapi.services.prono.count(ctx.query);
    },

    /**
     * Retrieve records.
     *
     * @return {Array}
     */

    async find(ctx) {
        let entities;

        ctx.query['competition.competition_users.user.id'] = ctx.state.user.id;
        ctx.query['competition.competition_users.Admin'] = true;

        if (ctx.query._q) {
            entities = await strapi.services['competition-users'].search(ctx.query);
        } else {
            entities = await strapi.services['competition-users'].find(ctx.query);
        }

        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models['competition-users'] }));
    },

    /**
     * Retrieve a record.
     *
     * @return {Object}
     */

    async findOne(ctx) {
        const { id } = ctx.params;

        const entity = await strapi.services['competition-users'].findOne({
            id,
            'competition.competition_users.user.id': ctx.state.user.id,
            'competition.competition_users.Admin': true,
        });

        return sanitizeEntity(entity, { model: strapi.models['competition-users'] });
    },
}